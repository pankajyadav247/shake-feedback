/*
 * Copyright 2016 Stuart Kent
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.celusion.feedbacklibrary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.util.Log;

import static android.content.Context.SENSOR_SERVICE;

/**
 * The main interaction point for library users. Encapsulates all shake detection. Setters allow users to customize some
 * aspects (recipients, subject line) of bug report emails.
 */
public final class ShakeFeedback implements ShakeDetector.Listener {

    @SuppressLint("StaticFieldLeak") // we're holding the application context.
    private static ShakeFeedback sharedInstance;

    private final Application application;
    private FeedbackFlowManager feedbackFlowManager;

    private static final String TAG = "ShakeFeedback-Library";

    // Instance configuration state:
    private boolean assembled = false;
    private boolean startAttempted = false;

    private final SimpleActivityLifecycleCallback simpleActivityLifecycleCallback = new SimpleActivityLifecycleCallback() {
        @Override
        public void onActivityResumed(final Activity activity) {
            feedbackFlowManager.onActivityResumed(activity);
        }

        @Override
        public void onActivityStopped(final Activity activity) {
            feedbackFlowManager.onActivityStopped();
        }
    };
    /**
     * @param application the embedding application
     * @return the singleton <code>ShakeFeedback</code> instance
     */
    @NonNull
    public static ShakeFeedback get(@NonNull final Application application) {
        synchronized (ShakeFeedback.class) {
            if (sharedInstance == null) {
                sharedInstance = new ShakeFeedback(application);
            }
        }

        return sharedInstance;
    }

    private ShakeFeedback(@NonNull final Application application) {
        this.application = application;
    }

    /**
     * (Required) Assembles dependencies based on provided configuration information. This method CANNOT be called more
     * than once. This method CANNOT be called after calling <code>start</code>.
     *
     * @return the current <code>ShakeFeedback</code> instance (to allow for method chaining)
     */
    @NonNull
    public ShakeFeedback assemble() {
        if (assembled) {
            Log.d(TAG,"You have already assembled this ShakeFeedback instance. Calling assemble again is a no-op.");
            return this;
        }

        if (startAttempted) {
            throw new IllegalStateException("You can only call assemble before calling start.");
        }

        feedbackFlowManager = new FeedbackFlowManager(
                application,
                new ActivityReferenceManager());

        assembled = true;
        return this;
    }

    /**
     * (Required) Start listening for device shaking. You MUST call <code>assemble</code> before calling this method.
     */
    public void start() {
        if (!assembled) {
            throw new IllegalStateException("You MUST call assemble before calling start.");
        }

        if (startAttempted) {
            Log.d(TAG,"You have already attempted to start this ShakeFeedback instance. Calling start "
                    + "again is a no-op.");

            return;
        }

        application.registerActivityLifecycleCallbacks(simpleActivityLifecycleCallback);

        final SensorManager sensorManager
                = (SensorManager) application.getSystemService(SENSOR_SERVICE);
        final ShakeDetector shakeDetector = new ShakeDetector(this);


        final boolean didStart = shakeDetector.start(sensorManager);

        if (didStart) {
            Log.d(TAG,"Shake detection successfully started!");
        } else {
            Log.d(TAG,"Error starting shake detection: hardware does not support detection.");
        }
        startAttempted = true;
    }

    @Override
    public void hearShake() {
        Log.d(TAG,"Shake detected!");

        feedbackFlowManager.startFlowIfNeeded();
    }
}
