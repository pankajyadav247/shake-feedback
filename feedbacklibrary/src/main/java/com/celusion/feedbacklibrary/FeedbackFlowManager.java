/*
 * Copyright 2016 Stuart Kent
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.celusion.feedbacklibrary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.celusion.feedbacklibrary.FeedbackActivity.FILE_PATH;


public final class FeedbackFlowManager {

    @NonNull
    private final Context applicationContext;

    @NonNull
    private final ActivityReferenceManager activityReferenceManager;


    public FeedbackFlowManager(
            @NonNull final Context applicationContext,
            @NonNull final ActivityReferenceManager activityReferenceManager) {

        this.applicationContext = applicationContext;
        this.activityReferenceManager = activityReferenceManager;
    }

    public void onActivityResumed(@NonNull final Activity activity) {

        activityReferenceManager.setActivity(activity);
    }

    public void onActivityStopped() {

    }

    public void startFlowIfNeeded() {
        showFeedbackAndScreenshot();
    }

    private void showFeedbackAndScreenshot() {
        final Activity currentActivity = activityReferenceManager.getValidatedActivity();
        if (currentActivity == null) {
            return;
        }

        takeScreenshot(currentActivity);
    }

    public static void takeScreenshot(@NonNull final Activity activity) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss.SS", Locale.getDefault());
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + dateFormat.format(new Date()) + ".jpg";

            View rootView = activity.getWindow().getDecorView().getRootView();

            /*rootView.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(rootView.getDrawingCache());
            rootView.setDrawingCacheEnabled(false);*/

            Bitmap bitmap = Bitmap.createBitmap(rootView.getWidth(), rootView.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            rootView.draw(canvas);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            sendFeedback(activity, imageFile);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static void sendFeedback(Context context, File imageFile) {

        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra(FILE_PATH, imageFile.getAbsolutePath());
        context.startActivity(intent);
    }

}
