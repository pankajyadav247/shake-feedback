package com.celusion.feedbacklibrary;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;



public class FeedbackActivity extends AppCompatActivity {

    public static final String FILE_PATH = "filepath";
    public static String postUrl;
    private TextInputEditText textInputEditTextRemark;
    private String localPath;
    private ImageView ivScreenshot;
    private ImageView ivClose;
    private RelativeLayout relativeLayoutScreenshot;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setMatBack();

        relativeLayoutScreenshot = (RelativeLayout) findViewById(R.id.relative_layout_screenshot);
        textInputEditTextRemark = (TextInputEditText) findViewById(R.id.text_input_description);
        ivScreenshot = (ImageView) findViewById(R.id.imageview_screenshot);
        ivClose = (ImageView) findViewById(R.id.imageview_close);
        ivClose.setOnClickListener(clkImage);
        ivScreenshot.setOnClickListener(clkImage);

        Intent intent = getIntent();
        if (intent.hasExtra(FILE_PATH)) {
            localPath = getIntent().getExtras().getString(FILE_PATH);
        }

        File file = new File(localPath);
        if (file.exists())
            ivScreenshot.setImageBitmap(BitmapFactory.decodeFile(localPath));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feedback_menu, menu);
        return true;
    }

    protected void setMatBack() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("Feedback");
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_send) {

            if (isValid()) {
              //  feedbackData.setRemark(textInputEditTextRemark.getText().toString());
                new SendPostRequest().execute();

            } else
                Toast.makeText(getApplicationContext(), "Enter your feedback to report us", Toast.LENGTH_LONG).show();
            return true;
        } else if (i == android.R.id.home) {
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private boolean isValid() {
        if (textInputEditTextRemark.getText().toString().trim().equals("")) {
            return false;
        }
        return true;
    }

    private View.OnClickListener clkImage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            int i = view.getId();
            if (i == R.id.imageview_screenshot) {
                openScreenshot();

            } else if (i == R.id.imageview_close) {
                closeScreenshot();

            }
        }
    };

    private void openScreenshot() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(new File(localPath));
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    private void closeScreenshot() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackActivity.this);
        builder.setMessage("Are you sure you want to delete screenshot ?")
                .setTitle("Confirm")
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        File file = new File(localPath);
                        boolean deleted = file.delete();
                        relativeLayoutScreenshot.setVisibility(View.GONE);

                    }
                })
                .setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public class SendPostRequest extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
        }

        protected String doInBackground(String... arg0) {

            try {

                URL url = new URL(postUrl); // here is your URL path

               JSONObject postDataParams = new JSONObject();
                /* postDataParams.put("appname", feedbackData.getAppname());
                postDataParams.put("packagename", feedbackData.getPackagename());
                postDataParams.put("version", feedbackData.getVersion());
                postDataParams.put("customdata", feedbackData.getCustomdata());
                postDataParams.put("remark", feedbackData.getRemark());
                postDataParams.put("image", feedbackData.getImage());
                postDataParams.put("platform", feedbackData.getPlatform());
                postDataParams.put("osversion", feedbackData.getOsversion());
                postDataParams.put("brand", feedbackData.getBrand());
                postDataParams.put("phonemodel", feedbackData.getPhonemodel());
                postDataParams.put("createdon", feedbackData.getCreatedon());
                postDataParams.put("latitude", feedbackData.getLatitude());
                postDataParams.put("longitude", feedbackData.getLongitude());
                Log.e("params", postDataParams.toString());*/

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}
