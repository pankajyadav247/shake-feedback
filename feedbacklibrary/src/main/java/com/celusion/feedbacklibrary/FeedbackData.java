package com.celusion.feedbacklibrary;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 * Created by administrator on 7/11/17.
 */

public class FeedbackData {

    private int id;
    private String appname;
    private String packagename;
    private String version;
    private String customdata;
    private String remark;
    private String image;
    private String platform;
    private String osversion;
    private String brand;
    private String phonemodel;
    private Date createdon;
    private double latitude;
    private double longitude;

    public FeedbackData() {
    }

    public FeedbackData(Context context,String imagePath) {
        setDeviceDetails(context, imagePath);
    }

    public FeedbackData(Context context, String remark, String imagePath) {
        this.remark = remark;
        setDeviceDetails(context, imagePath);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCustomdata() {
        return customdata;
    }

    public void setCustomdata(String customdata) {
        this.customdata = customdata;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPhonemodel() {
        return phonemodel;
    }

    public void setPhonemodel(String phonemodel) {
        this.phonemodel = phonemodel;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private void setDeviceDetails(Context context, String imagePath) {

        final PackageManager pm = context.getPackageManager();
        if (pm != null) {
            this.packagename = context.getApplicationContext().getPackageName();
        }
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(packagename, 0);
            this.appname = (String) ((applicationInfo != null) ? pm.getApplicationLabel(applicationInfo) : "???");
        } catch (final PackageManager.NameNotFoundException e) {
        }
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            if (info != null) {
                //this.versionname = info.versionName;
                this.version = String.valueOf(info.versionCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.phonemodel = android.os.Build.MODEL;
        this.brand = android.os.Build.MANUFACTURER;
        this.osversion = Build.VERSION.RELEASE;
        this.platform = "Android";

        Bitmap bm = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        this.image = Base64.encodeToString(b, Base64.DEFAULT);

    }


}
